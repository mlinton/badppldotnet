/* 
 * Dynamically load content from partial files in /content
 * Requires jQuery
 * 
 * URL ARGS:
 *      cn      file reference
 *      ln      subfolder reference
 */

(function($){ 
    const content = 'content/';
    const posts = content + 'posts/';
    
    // Get URL Params
    $.urlParam = function( name ) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return ( results !== null ? results[1] : 0 );
    };

    switch( $.urlParam('cn') ) {
        case 'about' :
            $( "#content" ).load( content + 'about.html' );
            break;
        default :
            $( "#content" ).load( content + 'home.html' );
    }
    
})(jQuery);
